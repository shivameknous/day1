from django.apps import AppConfig


class WhatsappTestConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'whatsapp_test'
