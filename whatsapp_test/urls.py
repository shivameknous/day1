from django.urls import path
from . import views


urlpatterns = [
    
    path('', views.whats_app, name="send_whatsapp"),
    path('home/', views.home, name="home"),
    path('email', views.mail_send, name='email'),
    path('email_send', views.test_mailsend, name='email_send'),

    
]